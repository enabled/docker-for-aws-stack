# Docker for AWS Stack

This repo forks the AWS CloudFormation Stack defined in Docker for AWS and adds outputs.

Original source: https://editions-us-east-1.s3.amazonaws.com/aws/stable/Docker.tmpl
https://docs.docker.com/docker-for-aws/

### Useful resources

#### Portainer

Docker for AWS CE doesn't have the pretty Web UI. Portainer does a pretty decent job for free however. It even lets you login to running containers all from the UI.

docker service create \
    --name portainer \
    --publish 9000:9000 \
    --constraint 'node.role == manager' \
    --mount type=bind,src=//var/run/docker.sock,dst=/var/run/docker.sock \
    portainer/portainer \
    -H unix:///var/run/docker.sock


#### Traefik

For Load balancing, ssl certs and a whole lot more.
https://traefik.io

docker service create \
--name traefik \
--constraint 'node.role==manager' \
--publish 80:80 --publish 443:443 --publish 8080:8080 \
--mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock \
--mount type=bind,source=/var/tmp,target=/etc/traefik/acme \
--network base-network \
traefik:1.3.0 \
--entryPoints='Name:http Address::80 Redirect.EntryPoint:https' \
--entryPoints='Name:https Address::443 TLS' \
--defaultEntryPoints=http,https \
--acme.entryPoint=https \
--acme.email=<email> \
--acme.storage=/etc/traefik/acme/acme.json \
--acme.domains=<domain> \
--acme.onHostRule=true \
--docker \
--docker.swarmmode \
--docker.domain=<domain> \
--docker.watch \
--logLevel=DEBUG \
--web
